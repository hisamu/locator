<?php 

namespace Codeinc\LocateBundle\Locator;

use Codeinc\LocateBundle\Engine\EngineInterface;

class Locator
{
    protected $engine;

    public function __construct($engine, $params = [])
    {
        $this->setEngine($engine, $params);
    }

    public function locate($term)
    {
        return $this->engine->locate($term);
    }

    public function setEngine($engine, $params)
    {
        $class = sprintf('Codeinc\\LocateBundle\\Engine\\%sEngine', ucfirst(preg_replace_callback('/[_-](.?)/', function($matches){
            return ucfirst($matches[1]);
        }, $engine)));

        if (!class_exists($class)) {
            throw new \Exception(sprintf('Unsupported engine: %s', $class));
        }

        $params = isset($params[$engine]) ? $params[$engine] : [];

        $this->engine = new $class($params);
    }

    public function getEngine()
    {
        return $this->engine;
    }
}
