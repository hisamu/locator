<?php 

namespace Codeinc\LocateBundle\Engine;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class FinderEngine extends AbstractEngine implements EngineInterface
{
    public function locate($term)
    {
        $results = new ArrayCollection();

        $finder = new Finder();
        $finder->files()->in($this->getParameter('folder'));

        foreach ($finder as $file) {
            if ($result = $this->processFile($file, $term)) {
                $results->add($result);
            }
        }

        return $results;
    }

    protected function processFile(SplFileInfo $file, $term)
    {
        $text = null;
        $contents = $file->getContents();

        if (stripos($contents, $term) !== false) {
            return $file->getRelativePathname();
        }

        return null;
    }
}
