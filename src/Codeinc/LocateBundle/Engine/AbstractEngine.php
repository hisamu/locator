<?php 

namespace Codeinc\LocateBundle\Engine;

use Codeinc\LocateBundle\Locator\Locator;

abstract class AbstractEngine
{
    protected $params;
    
    public function __construct($params)
    {
        $this->params = $params;
    }

    public function getParameter($param)
    {
        return isset($this->params[$param]) ? $this->params[$param] : null;
    }
}
