<?php 

namespace Codeinc\LocateBundle\Engine;

interface EngineInterface 
{
    public function locate($term);
}
