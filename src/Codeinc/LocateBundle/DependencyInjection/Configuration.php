<?php

namespace Codeinc\LocateBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('codeinc_locate');

        $rootNode->children()
            ->scalarNode('engine')
                ->defaultValue('finder')
                ->cannotBeEmpty()
            ->end()
            ->arrayNode('params')
                ->children()
                    ->arrayNode('finder')
                        ->children()
                            ->scalarNode('folder')
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
