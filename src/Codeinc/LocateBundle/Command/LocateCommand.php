<?php 

namespace Codeinc\LocateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LocateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('codeinc:locator')
            ->setDescription('Searchs through files with your favorite engine')
            ->addOption('term', null, InputOption::VALUE_REQUIRED, 'Search term')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $term = $input->getOption('term');

        $locator = $this->getContainer()->get('codeinc.locator');

        foreach ($locator->locate($term) as $result) {
            $output->writeln(sprintf('Result found: <info>%s</info>', $result));
        }
    }
}
