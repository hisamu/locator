<?php 

namespace Codeinc\LocateBundle\Tests\Engine;

use Codeinc\LocateBundle\Engine\FinderEngine;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;

class FinderEngineTest extends \PHPUnit_Framework_TestCase
{
    public function testLocate()
    {
        $engine = new FinderEngine([
            'folder' => __DIR__.'/../Fixtures',
        ]);

        $results = $engine->locate('ipsum');

        $this->assertNotEmpty($results);
    }
}
