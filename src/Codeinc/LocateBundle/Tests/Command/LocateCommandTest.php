<?php

namespace Codeinc\LocateBundle\Tests\Controller;

use Codeinc\LocateBundle\Command\LocateCommand;
use Codeinc\LocateBundle\Locator\Locator;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;


class LocateCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = $this->createKernel();
        $kernel->boot();

        $kernel->getContainer()->set('codeinc.locator', new Locator('finder', [
            'finder' => ['folder' => __DIR__.'/../Fixtures'],
        ]));

        $application = new Application($kernel);
        $application->add(new LocateCommand());

        $command = $application->find('codeinc:locator');
        $tester = new CommandTester($command);

        $tester->execute([
            'command' => $command->getName(),
            '--term' => 'ipsum',
        ]);

        $this->assertRegExp('/Result found/', $tester->getDisplay());
    }
}
