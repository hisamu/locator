<?php 

namespace Codeinc\LocateBundle\Tests\Locator;

use Codeinc\LocateBundle\Locator\Locator;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;

class LocatorTest extends \PHPUnit_Framework_TestCase
{
    protected function getLocator()
    {
        return new Locator('finder', [
            'finder' => ['folder' => __DIR__.'/../Fixtures'],
        ]);
    }

    public function testEngine()
    {
        $locator = $this->getLocator();

        $this->assertInstanceOf('Codeinc\LocateBundle\Engine\FinderEngine', $locator->getEngine());
    }

    public function testLocate()
    {
        $locator = $this->getLocator();

        $results = $locator->locate('ipsum');

        $this->assertNotEmpty($results);
    }
}
