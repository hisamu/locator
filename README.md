Codeinc Locator
=======

This is a command line utility to search file contents. The search engine can be extended, but by default it only support basic file search through Symfony's Finder component.

Assuming that your `/files` directory has several files and you want to search them:

**Configuration**

    # config.yml
    codeinc_locate:
        engine: finder
        params:
            finder:
                folder: "%kernel.root_dir%/../files"

**Command**

`$ php bin/console codeinc:locator --term='ipsum'`

The output would be:

`Result found: lipsum.txt`
